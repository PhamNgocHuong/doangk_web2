﻿using DoAn_Web_1460440.Areas.Admin.Models.AdminBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAn_Web_1460440.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin/Admin
        public ActionResult Index()
        {
            var sp = SanPhamAdminBus.DanhSachAdmin();
            return View(sp);
        }

        // GET: Admin/Admin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Admin/Create
        [HttpPost]
        public ActionResult Create(BaByShopConnection.sanpham sp)
        {
               var hpf = HttpContext.Request.Files[0];
                if (hpf.ContentLength > 0)
                {
                    string fileName = Guid.NewGuid().ToString();

                    string fullPathWithFileName = "/images/products/" + fileName + ".jpg";
                    hpf.SaveAs(Server.MapPath(fullPathWithFileName));
                    sp.HinhAnh = fullPathWithFileName;
                }
            

                // TODO: Add insert logic here
            SanPhamAdminBus.Them(sp);
                return RedirectToAction("Index");
            
            
        }

        // GET: Admin/Admin/Edit/5
        public ActionResult Edit(int id)
        {
            return View(SanPhamAdminBus.ChiTiet(id));
            
        }

        // POST: Admin/Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, BaByShopConnection.sanpham collection)
        {
            try
            {
                // TODO: Add update logic here
                SanPhamAdminBus.Update(id, collection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Admin/Delete/5
        public ActionResult Delete(int id)
        {
            return View(SanPhamAdminBus.ChiTiet(id));
        }

        // POST: Admin/Admin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, BaByShopConnection.sanpham collection)
        {
            try
            {
                // TODO: Add delete logic here
                SanPhamAdminBus.Delete(id, collection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
