﻿using System.Web;
using System.Web.Mvc;

namespace DoAn_Web_1460440
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
