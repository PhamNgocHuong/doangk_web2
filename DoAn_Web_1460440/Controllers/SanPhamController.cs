﻿using DoAn_Web_1460440.Models.Bus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAn_Web_1460440.Controllers
{
    public class SanPhamController : Controller
    {
        // GET: SanPham
        public ActionResult Index(int page = 1)
        {
            var DanhSachSanPham = SanPhamBus.DanhSach(page, 6);
            return View(DanhSachSanPham);
        }

        // GET: SanPham/Details/5
        public ActionResult Details(int id)
        {
            return View(SanPhamBus.ChiTiet(id));
        }

        // GET: SanPham/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SanPham/Create
        [HttpPost]
        public ActionResult Create(BaByShopConnection.sanpham sp)
        {
               
                return RedirectToAction("Index");
           
        }

        // GET: SanPham/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SanPham/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, BaByShopConnection.sanpham sp)
        {
           
            return RedirectToAction("Index");

        }

        // GET: SanPham/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SanPham/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
