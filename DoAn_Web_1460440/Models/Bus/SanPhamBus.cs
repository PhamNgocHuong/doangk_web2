﻿using BaByShopConnection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAn_Web_1460440.Models.Bus
{
    public class SanPhamBus
    {
        public static IEnumerable<sanpham> DanhSach()
        {
            var db = new BaByShopConnectionDB();
            return db.Query<BaByShopConnection.sanpham>("select * from sanpham");
        }
        public static Page<sanpham> DanhSach(int pageNumeBer, int itemsPage)
        {
            var db = new BaByShopConnectionDB();
            return db.Page<sanpham>(pageNumeBer, itemsPage,"select * from sanpham");
        }
        public static BaByShopConnection.sanpham ChiTiet(int id)
        {
            var db = new BaByShopConnectionDB();
            return db.SingleOrDefault<BaByShopConnection.sanpham>("select * from sanpham where masanpham = @0", id);
        }
        public static void Them(BaByShopConnection.sanpham sp)
        {
            var db = new BaByShopConnectionDB();
            db.Insert(sp);
        }
        public static void Update(int id, BaByShopConnection.sanpham sp)
        {
            var db = new BaByShopConnectionDB();
            db.Update<sanpham>("SET TenSanPham = @0, MoTa = @1, XuatXu = @2, MaNhaSanXuat = @3, GiaBan = @4, SoLuongBan = @5, SoLuongTon = @6, MaLoaiSanPham = @7, HinhAnh = @8 where MaSanPham = @9", sp.TenSanPham, sp.MoTa, sp.XuatXu, sp.MaNhaSanXuat, sp.GiaBan, sp.SoLuongBan, sp.SoLuongTon, sp.MaLoaiSanPham, sp.HinhAnh, id);
        }
    }
}