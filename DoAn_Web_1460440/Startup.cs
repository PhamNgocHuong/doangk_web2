﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DoAn_Web_1460440.Startup))]
namespace DoAn_Web_1460440
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
